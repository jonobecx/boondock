FROM alpine

RUN apk update
RUN apk add libressl-dev openssl

WORKDIR /etc
ADD https://github.com/camchenry/boon/releases/download/master/boon-master-x86_64-unknown-linux-gnu.tar.gz .
RUN tar -zxvf boon-master-x86_64-unknown-linux-gnu.tar.gz
RUN chmod 755 /etc/boon/boon

ENTRYPOINT [ "/etc/boon/boon" ]
