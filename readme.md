# Boondock

A docker image for building Love2D games in a CI environment. "Official" builds of the image can be pulled from `cenetex/boondock`

[Boon](https://github.com/camchenry/boon) is a build tool for Love2D.

## Example

This is an example repository using boondock and Bitbucket pipelines to build and publish a Love2D game：

https://bitbucket.org/jonobecx/asteroids.love/